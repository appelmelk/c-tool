What it does:
- Calculate the average profit on a monthly basis
- Calculate the standard deviation

Add this deviation to the price on Monday after exp friday
66% chance



default:
gcc  -Wall  main.c -o stock -lm

with debug:
gcc -DDEBUG -Wall  main.c -o stock -lm

Todo:
add mean / variance calculation to seperate library `basic_statistics'
clean up `process_source_file'
add Makefile

Long term:
Intergrate with gnuplot
