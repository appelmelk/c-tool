#include <math.h>

#if defined(DEBUG) && DEBUG > 0
#define DEBUG_PRINT(fmt, args...) fprintf(stderr, "DEBUG: %s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##args)
#else
#define DEBUG_PRINT(fmt, args...) /* Don't do anything in release builds */
#endif

float calculate_mean(float **grid, int size)
{
    float mean = 0;
    int i = 0;

    for (i = 1; i < size; i++) {
        grid[i][2] = ((grid[i-1][0] - grid[i][1]) / grid[i-1][0]) * 100;
    }

    for (i = 1; i < size; i++) {
        mean = mean + grid[i][2];
    }

    return (mean / size - 1);
}

float calculate_variance(float **grid, int size, float mean)
{
    float variance = 0;
    float diff;

    int i;

    for (i = 1; i < size; i++) {
        diff = grid[i][2] - mean;
        variance = variance + (diff * diff);
    }

    return fabsf(variance / size);
}
