#Constants used on this make file
CC=gcc
CFLAGS=-Wall -static
LIBS=-lm
LINKFLAG=-Llibs -lstatics 
DIRS=libs

all: subsystem main 
	
subsystem:
	cd libs && $(MAKE)


main: main.c
	$(CC) $(CFLAGS)  main.c $(LINKFLAG) -o stock $(LIBS)

clean:
	for d in $(DIRS); do (cd $$d; $(MAKE) clean ); done
	rm -rf *.o stock  


	
