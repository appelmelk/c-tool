#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>
#include "./libs/statistics.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#define MAX_ELEMENTS    	6
#define CLOSE_ELEMENT   	5
#define DATE_ELEMET     	1

#define BUFFER_SIZE 1024

#if defined(DEBUG) && DEBUG > 0
#define DEBUG_PRINT(fmt, args...) fprintf(stderr, "DEBUG: %s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ##args)
#else
#define DEBUG_PRINT(fmt, args...) /* Don't do anything in release builds */
#endif

/*
 * Find the day of the week
 */

int day_of_week(int d, int m, int y)
{
    static int t[] = { 0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4 };
    y -= m < 3;
    return ( y + y/4 - y/100 + y/400 + t[m-1] + d) % 7;
}

void get_date_from_date_token(int *day, int *month, int *year, char *date)
{

    int tot_sub_elements = 3, day_element = 2, month_element = 1, year_element = 0;
    char *dates[3] = { 0 };
    int i = 0;

    dates[i] = strtok(date, "-");

    for (i = 1; i < tot_sub_elements; i++) {
        dates[i]=strtok(NULL, "-");
    }

    *day = atoi(dates[day_element]);
    *month = atoi(dates[month_element]);
    *year = atoi(dates[year_element]);
}

void get_elements_from_line(float *closing_price, char **date, char *line)
{

    int i;
    char *element[MAX_ELEMENTS] = { 0 };

    *date = strtok(line, ",");

    for (i = 0; i < MAX_ELEMENTS; i++) {
        element[i]=strtok(NULL, ",");

        DEBUG_PRINT("Token value = %s; Index =  %d\n", element[i], i );
    }

    *closing_price = atof(element[CLOSE_ELEMENT]);
}

int is_expiration_date (char *date)
{

    int day = 0, month = 0, year = 0, weekday = 0;

    get_date_from_date_token(&day, &month, &year, date);
    weekday = day_of_week(day, month, year);

    return (weekday == 5 && day >= 15 && day <= 21) ? 0 : -1;
}

FILE *open_file(char *filename, char *mode)
{

    FILE *fp = fopen(filename, mode);

    if (fp == NULL) {
        fprintf(stderr, "Error opening file: %s\n", strerror( errno ));
        return NULL;
    } else {
        DEBUG_PRINT("Opening File: %s Successfull\n", filename);
    }

    return fp;
}

int count_file_lines(char *filename, char *mode)
{

    FILE *fp = open_file(filename, mode);

    int lines = 0;
    char ch;

    while(!feof(fp)) {
        ch = fgetc(fp);

        if(ch == '\n')
            lines++;
    }

    fclose(fp);
    return lines;
}

int malloc_grid(float ***grid, int  nrows, int ncols)
{
    int i;
    *grid = malloc( sizeof(float *) * nrows);

    if (*grid == NULL) {
        printf("ERROR: out of memory\n");
        return 1;
    }

    for (i = 0; i < nrows; i++) {
        (*grid)[i] = malloc( sizeof(float) * ncols);

        if ((*grid)[i] == NULL) {
            printf("ERROR: out of memory\n");
            return 1;
        }
    }

    return 0;
}

void print_grid(float **grid, int  nrows, int ncols)
{
    int i, j;

    for (i = 0; i < nrows; i++) {
        for (j = 0; j < ncols; j++) {
            printf ("%f\t", grid[i][j] );
        }
        printf("\n");
    }
}

/*
    to do: clean up...

*/

void process_source_file(char *filename)
{
    FILE *fp;
    char *mode = "r";
    char *current, *previous;
    char *date;
    char current_line[BUFFER_SIZE], previous_line[BUFFER_SIZE];

    float closing_price = 0;
    float **grid;
    float mean = 0, variance = 0, deviation = 0;

    int is_first = 0;
    int nr_of_lines;
    int i = 0;
    int exp_counter = 0;

    DEBUG_PRINT("Going to open: %s in \"%s\" mode\n", filename, mode);

    nr_of_lines = count_file_lines(filename, mode);

    DEBUG_PRINT("File Size = %d\n", nr_of_lines);

    if (malloc_grid(&grid, nr_of_lines, 3) != 0) {
        DEBUG_PRINT("Failed to allocate Memory\n");
        exit(-1);
    }

    if ((fp = open_file(filename, mode)) == NULL ) {
        exit(-1) ;
    }

    while (fgets(current_line, BUFFER_SIZE, fp)) {

        current = (char *) strdup(current_line);
        previous = (char *) strdup(previous_line);

        if (is_first == 1) {

            DEBUG_PRINT("Current Content:%s\n", current_line);
            DEBUG_PRINT("Previous Content:%s\n", previous_line);

            get_elements_from_line(&closing_price, &date, current);

            if(is_expiration_date(date) == 0 ) {

                grid[i][0] = closing_price;

                get_elements_from_line(&closing_price, &date, previous);

                grid[i][1] = closing_price;

                i++;
                exp_counter++;
            }

        } else {
            is_first = 1;
        }

        strcpy(previous_line, current_line);
    }

    mean = calculate_mean(grid, exp_counter);
    variance = calculate_variance(grid, exp_counter, mean);
    deviation = sqrt(variance);

    print_grid(grid, exp_counter, 3);

    DEBUG_PRINT("Counter:%d\n", exp_counter);
    DEBUG_PRINT("Variance:%f\n", variance);

    printf(ANSI_COLOR_RED "The Mean = %f and Deviation = %f\n" ANSI_COLOR_RESET, mean, deviation);

    free(current);
    free(previous);
    fclose(fp);
    free(grid);
}

int main(int argc, char *argv[])
{

    int i = 0;
    char *filename;

    for (i = 0; i < argc; i++) {
        if (strcmp(argv[i], "--file") == 0) {
            filename = argv[++i];
            DEBUG_PRINT("The filename supplier is: %s\n", filename);
        }
    }

    process_source_file(filename);

    return 0;
}

